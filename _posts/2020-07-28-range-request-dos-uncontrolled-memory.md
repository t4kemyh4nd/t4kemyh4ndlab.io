---
layout: post
title: 'Range Request DoS: An uncontrolled memory consumption vector in Go''s net/http'
date: '2020-07-28T23:06:00.003-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2020-09-10T23:15:25.082-07:00'
thumbnail: https://miro.medium.com/max/1920/0*wGvcy48A6bfnv1-C.png"
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-1691634635148655824
blogger_orig_url: http://blog.takemyhand.xyz/2020/07/range-request-dos-uncontrolled-memory.html
---
<div id="html" markdown="0">
<div dir="ltr" style="text-align: left;" trbidi="on">
<br />
<div dir="ltr" style="text-align: left;" trbidi="on">
In this blog I highlight a very old DoS technique, which works via usage of HTTP's range requests, which I have dubbed RRDoS for the rest of the post [short for Range Request Denial of Service]. This is not to be confused with ReDoS, which depends on misconfigured regex.<br />
<h2 style="text-align: left;">
What are Range requests?</h2>
<div>
Let's first understand what range requests are. Simply <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests" rel="nofollow" target="_blank">put by MDN</a>-&nbsp;HTTP range requests allow to send only a portion of an HTTP message from a server to a client. Partial requests are useful for large media or downloading files with pause and resume functions, for example.</div>
<div>
<br /></div>
<div>
As an example, if there is an endpoint which serves an image at path <b>/static/cat.jpg</b> whose size is 1024 bytes, then we can request the first 10 bytes only by sending a request like:</div>
<div>
<pre class="line-numbers language-html" style="-webkit-hyphens: none; background-color: #eeeeee; border-color: rgb(61, 126, 154); border-style: solid; border-width: 0px 0px 0px 5px; box-sizing: border-box; caret-color: rgb(51, 51, 51); color: #333333; counter-reset: linenumber 0; direction: ltr; font-family: consolas, monaco, &quot;andale mono&quot;, monospace; font-size: 16px; letter-spacing: -0.04447999969124794px; line-height: 1.5; margin-bottom: 20px; max-width: 100%; overflow: auto; padding: 15px 15px 15px 3.8em; position: relative; tab-size: 4; text-shadow: none; width: 785.25px; word-break: normal; word-wrap: normal;"><code class="language-html" style="-webkit-hyphens: none; background-color: transparent; background-position: 0px 0px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; border-top-left-radius: 2px; border-top-right-radius: 2px; border: 0px; font-family: consolas, monaco, &quot;andale mono&quot;, &quot;ubuntu mono&quot;, monospace; font-size: 1em; line-height: 1.5; margin: 0px; padding: 0px; position: relative; tab-size: 4; text-shadow: none; white-space: inherit; word-break: normal; word-spacing: normal; word-wrap: normal;">GET /static/cat.jpg HTTP/1.1
Host: www.test.com
Range: bytes=0-10</code></pre>
</div>
<div>
Now if the server accepts range requests, it will reply with something like:&nbsp;</div>
<div>
<pre class="line-numbers language-html" style="-webkit-hyphens: none; background-color: #eeeeee; border-color: rgb(61, 126, 154); border-style: solid; border-width: 0px 0px 0px 5px; box-sizing: border-box; caret-color: rgb(51, 51, 51); color: #333333; counter-reset: linenumber 0; direction: ltr; font-family: consolas, monaco, &quot;andale mono&quot;, monospace; font-size: 16px; letter-spacing: -0.04447999969124794px; line-height: 1.5; margin-bottom: 20px; max-width: 100%; overflow: auto; padding: 15px 15px 15px 3.8em; position: relative; tab-size: 4; text-shadow: none; width: 785.25px; word-break: normal; word-wrap: normal;"><code class="language-html" style="-webkit-hyphens: none; background-color: transparent; background-position: 0px 0px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; border-top-left-radius: 2px; border-top-right-radius: 2px; border: 0px; font-family: consolas, monaco, &quot;andale mono&quot;, &quot;ubuntu mono&quot;, monospace; font-size: 1em; line-height: 1.5; margin: 0px; padding: 0px; position: relative; tab-size: 4; text-shadow: none; white-space: inherit; word-break: normal; word-spacing: normal; word-wrap: normal;">HTTP/1.1 206 Partial Content
Content-Range: bytes 0-10/1024
Content-Length: 10</code></pre>
</div>
<div>
Now that we know what range requests are, let's try to understand the attack.</div>
<h2 style="text-align: left;">
Multipart ranges</h2>
<div>
Using range requests, it is also possible for one to request multiple ranges for a resource, by sending a request like:</div>
<div>
<pre class="line-numbers language-html" style="-webkit-hyphens: none; background-color: #eeeeee; border-color: rgb(61, 126, 154); border-style: solid; border-width: 0px 0px 0px 5px; box-sizing: border-box; caret-color: rgb(51, 51, 51); color: #333333; counter-reset: linenumber 0; direction: ltr; font-family: consolas, monaco, &quot;andale mono&quot;, monospace; font-size: 16px; letter-spacing: -0.04447999969124794px; line-height: 1.5; margin-bottom: 20px; max-width: 100%; overflow: auto; padding: 15px 15px 15px 3.8em; position: relative; tab-size: 4; text-shadow: none; width: 785.25px; word-break: normal; word-wrap: normal;"><code class="language-html" style="-webkit-hyphens: none; background-color: transparent; background-position: 0px 0px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; border-top-left-radius: 2px; border-top-right-radius: 2px; border: 0px; font-family: consolas, monaco, &quot;andale mono&quot;, &quot;ubuntu mono&quot;, monospace; font-size: 1em; line-height: 1.5; margin: 0px; padding: 0px; position: relative; tab-size: 4; text-shadow: none; white-space: inherit; word-break: normal; word-spacing: normal; word-wrap: normal;">GET /static/cat.jpg HTTP/1.1
Host: www.test.com
Range: bytes=0-10,10-1024</code></pre>
</div>
<div style="text-align: left;">
The server will respond to this request by sending:</div>
<pre class="line-numbers language-html" style="-webkit-hyphens: none; background-color: #eeeeee; border-color: rgb(61, 126, 154); border-style: solid; border-width: 0px 0px 0px 5px; box-sizing: border-box; caret-color: rgb(51, 51, 51); counter-reset: linenumber 0; direction: ltr; font-family: consolas, monaco, &quot;andale mono&quot;, monospace; font-size: 16px; letter-spacing: -0.04447999969124794px; line-height: 1.5; margin-bottom: 20px; max-width: 100%; overflow: auto; padding: 15px 15px 15px 3.8em; position: relative; tab-size: 4; text-shadow: none; width: 918.75px; word-break: normal; word-wrap: normal;"><code class="language-html" style="-webkit-hyphens: none; background-color: transparent; background-position: 0px 0px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; border-top-left-radius: 2px; border-top-right-radius: 2px; border: 0px; font-family: consolas, monaco, &quot;andale mono&quot;, &quot;ubuntu mono&quot;, monospace; font-size: 1em; line-height: 1.5; margin: 0px; padding: 0px; position: relative; tab-size: 4; text-shadow: none; white-space: inherit; word-break: normal; word-spacing: normal; word-wrap: normal;"><span style="color: #333333;">HTTP/1.1 206 Partial Content
Content-Type: multipart/byteranges; boundary=3d6b6a416f9b5
Content-Length: 282

--3d6b6a416f9b5
Content-Type: image/jpeg
Content-Range: bytes 0-10/1024
    </span><span class="token tag" style="border: 0px; margin: 0px; padding: 0px;"><b><span class="token tag" style="border: 0px; margin: 0px; padding: 0px;"><span class="token punctuation" style="border: 0px; color: #999999; margin: 0px; padding: 0px;">&lt;</span><span style="color: #741b47;">binary content here</span></span><span class="token punctuation" style="border: 0px; color: #999999; margin: 0px; padding: 0px;">&gt;</span></b></span><span style="color: #333333;">
--3d6b6a416f9b5
Content-Type: image/jpeg
Content-Range: bytes 10-1024/1024
    </span><b><span style="color: #999999;">&lt;</span><span style="color: #741b47;">binary content here</span><span style="color: #999999;">&gt;</span></b><span style="color: #333333;">
--3d6b6a416f9b5--</span></code></pre>
<h2 style="text-align: left;">
The attack</h2>
<div>
RFC 7233 lays down the rules and semantics for use of range requests by servers and clients as well. If you scroll down to the <a href="https://tools.ietf.org/html/rfc7233#section-6.1" target="_blank">Security Considerations</a> section, you will see a small paragraph which says:</div>
<div>
&nbsp;&nbsp;</div>
<div>
<pre class="newpage" style="break-before: page;"><b>"   Unconstrained multiple range requests are susceptible to denial-of-
   service attacks because the effort required to request many
   overlapping ranges of the same data is tiny compared to the time,
   memory, and bandwidth consumed by attempting to serve the requested
   data in many parts.  Servers ought to ignore, coalesce, or reject
   egregious range requests, such as requests for more than two
   overlapping ranges or for many small ranges in a single set,
   particularly when the ranges are requested out of order for no
   apparent reason.  Multipart range requests are not designed to
   support random access.   "</b></pre>
<pre class="newpage" style="break-before: page;"><b>
</b></pre>
This is self-explanatory. Basically, if you request many overlapping ranges recursively, the server may try to parse EACH of these ranges individually, leading to a potential memory consumption, and eventually crashing the server.<br />
<br /></div>
<div>
An example malicious request for the cat.jpg file above would look like:</div>
<div>
<pre class="line-numbers language-html" style="-webkit-hyphens: none; background-color: #eeeeee; border-color: rgb(61, 126, 154); border-style: solid; border-width: 0px 0px 0px 5px; box-sizing: border-box; caret-color: rgb(51, 51, 51); color: #333333; counter-reset: linenumber 0; direction: ltr; font-family: consolas, monaco, &quot;andale mono&quot;, monospace; font-size: 16px; letter-spacing: -0.04447999969124794px; line-height: 1.5; margin-bottom: 20px; max-width: 100%; overflow: auto; padding: 15px 15px 15px 3.8em; position: relative; tab-size: 4; text-shadow: none; width: 785.25px; word-break: normal; word-wrap: normal;"><code class="language-html" style="-webkit-hyphens: none; background-color: transparent; background-position: 0px 0px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; border-top-left-radius: 2px; border-top-right-radius: 2px; border: 0px; font-family: consolas, monaco, &quot;andale mono&quot;, &quot;ubuntu mono&quot;, monospace; font-size: 1em; line-height: 1.5; margin: 0px; padding: 0px; position: relative; tab-size: 4; text-shadow: none; white-space: inherit; word-break: normal; word-spacing: normal; word-wrap: normal;">GET /static/cat.jpg HTTP/1.1
Host: www.test.com
Range: bytes=0-5,0-7,1-6,9-12,10-11,4-5,0-9,0-10,0-12,...</code></pre>
</div>
<div>
If the server is vulnerable to the above attack, it creates pieces of a multipart response for each range specified. That eats up both memory and CPU on the server, and doing so tens or hundreds of times for multiple attacker connections is enough to exhaust the server resources and cause the DoS. The range requests in the attack are obviously not reasonable, but they are legal according to the HTTP specification. Apache and Squid have been found vulnerable to this attack in the past. (<a href="https://lwn.net/Articles/456723/" target="_blank">reference here</a>)</div>
<h2 style="text-align: left;">
Attacking Go's net/http</h2>
<div>
Now that we know what a textbook RRDoS attack request looks like, let's try to reproduce it in Go's net/http package.</div>
<div>
<br /></div>
<div>
We create a server with the following code:<br />
<br /></div>
<div>
<table class="highlight tab-size js-file-line-container" data-paste-markdown-skip="" data-tab-size="8" style="border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; caret-color: rgb(36, 41, 46); color: #24292e; font-size: 14px; tab-size: 8;"><tbody style="box-sizing: border-box;">
<tr style="box-sizing: border-box;"></tr>
<tr style="box-sizing: border-box;"><td class="blob-code blob-code-inner js-file-line highlighted" id="LC17" style="box-sizing: border-box; line-height: 20px; overflow: visible; padding: 0px 10px; position: relative; vertical-align: top; white-space: pre; word-wrap: normal;"><span style="font-size: small;"><span style="background-color: white;"><span class="pl-s1" style="box-sizing: border-box;">-<b style="font-style: italic;"> http</b></span><b style="font-style: italic;">.</b><span class="pl-en" style="box-sizing: border-box; color: #6f42c1; font-style: italic; font-weight: bold;">Handle</span><b style="font-style: italic;">(</b><span class="pl-s" style="box-sizing: border-box; color: #032f62; font-style: italic; font-weight: bold;">"/static/"</span><b style="font-style: italic;">, </b><span class="pl-s1" style="box-sizing: border-box; font-style: italic; font-weight: bold;">http</span><b style="font-style: italic;">.</b><span class="pl-en" style="box-sizing: border-box; color: #6f42c1; font-style: italic; font-weight: bold;">StripPrefix</span><b style="font-style: italic;">(</b><span class="pl-s" style="box-sizing: border-box; color: #032f62; font-style: italic; font-weight: bold;">"/static/"</span><b style="font-style: italic;">, </b><span class="pl-s1" style="box-sizing: border-box; font-style: italic; font-weight: bold;">http</span><b style="font-style: italic;">.</b><span class="pl-en" style="box-sizing: border-box; color: #6f42c1; font-style: italic; font-weight: bold;">FileServer</span><b style="font-style: italic;">(</b><span class="pl-s1" style="box-sizing: border-box; font-style: italic; font-weight: bold;">http</span><b style="font-style: italic;">.</b><span class="pl-en" style="box-sizing: border-box; color: #6f42c1; font-style: italic; font-weight: bold;">Dir</span><b style="font-style: italic;">(</b><span class="pl-s" style="box-sizing: border-box; color: #032f62; font-style: italic; font-weight: bold;">"./"</span><b style="font-style: italic;">))))</b></span></span></td></tr>
</tbody></table>
</div>
<div>
<br />
This simply serves any content inside the <b>/static/</b> directory.</div>
<div>
<br /></div>
<div>
Now when we try to send a malicious range request to the <b>/static</b> directory using a lot of concurrent threads, we are clearly able to see that the server lags out quite a lot in response to it. However, when we read the response that we finally get, we see that the server did NOT send a response to the Range request. It merely sends us the complete image file, without the multipart stuff.&nbsp;</div>
<div>
<br /></div>
<div>
If this is happening, and the server isn't responding to the Range request, then why does the server take so much time to respond to the request?</div>
<div>
<br /></div>
<div>
The answer lies in the code written in Go for handling Range requests in the net/http/fs.go file.</div>
<script src="https://gist.github.com/t4kemyh4nd/e54d9382d4f2c1aaf5c018d72c73a2d5.js"></script>

<br />
<div>
If you see, on line 10 of the above code, there indeed exists a check for this exact attack by use of the <b>sumRangesSize()</b> function. Then why are we still getting uncontrolled memory consumption?<br />
<br />
So whats happening is, on line 2 of the above code, there is a function called <b>parseRange()</b>, which is still parsing the Range request sent. If you look at the code for the <b>parseRange()</b> function, it processes the values in the Range header, and simultaneously creates an array of type&nbsp;<b>[]httpRange</b>, whose length is equal to the different ranges specified in the Range header (eg. 1-5, 20-25 etc.) to hold the requested bytes.<br />
<script src="https://gist.github.com/t4kemyh4nd/770cdd9092bea1e5ecf769d399a9269e.js"></script>
Even though these bytes aren't pushed to the array, the array is still declared as a placeholder for these values, which takes up a lot of memory allocation.<br />
<br />
And this is why we see a rise in the memory consumption of the server, when sending a lot of these malicious Range requests on several concurrent threads to a server.</div>
</div>
</div>
</div>