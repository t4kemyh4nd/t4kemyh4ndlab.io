---
layout: post
title: Escalating subdomain takeovers to steal cookies by abusing document.domain
date: '2019-05-23T23:44:00.002-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2020-04-25T06:20:38.513-07:00'
thumbnail: https://www.wallpaperflare.com/static/846/199/701/dark-keyboards-macro-lights-wallpaper.jpg
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-419460270844889958
blogger_orig_url: http://blog.takemyhand.xyz/2019/05/escalating-subdomain-takeovers-to-steal.html
---

<div id="html" markdown="0">
<div dir="ltr" style="text-align: left;" trbidi="on">
<br />
<div dir="ltr" style="text-align: left;" trbidi="on">
<div dir="ltr" style="text-align: left;" trbidi="on">
<div dir="ltr" style="text-align: left;" trbidi="on">
<span style="font-size: x-large;">H</span>i everyone,<br />
<br />
It's been a really long time since I've blogged about anything. Mainly because I got a job as security analyst- therefore I've been very busy. And I also got into Synack!<br />
<br />
This blog post focuses on one of my recent findings, how I was able to escalate an out of scope subdomain takeover to steal the session cookies. And how I also thought of a few more methods to escalate it, affecting the main domain.<br />
<br />
Before I get started with the bug, I would like to talk a little about same-origin policy. If 2 websites want to communicate with each other, or perhaps if 2 hosts want to <i>interact</i> with each other, there could be mainly 2 ways to go around this. The book "Tangled Web" (a great book, do read it) states the following:<br />
<br />
<i>"<span style="font-family: &quot;newbaskerville&quot;;">Attempts to broaden origins or facilitate cross-domain interactions are more common. The two broadly sup- ported ways of achieving these goals are&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">document.domain&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">and&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">postMessage(...)"</span></i><br />
<h2 style="text-align: left;">
<span style="font-family: &quot;arial&quot; , &quot;helvetica&quot; , sans-serif; font-size: x-large;">DOCUMENT.DOMAIN</span></h2>
<div class="page" title="Page 165">
<div class="layoutArea">
<div class="column">
<span style="font-family: &quot;newbaskerville&quot;;">This JavaScript property permits any two cooperating websites that share a common top-level domain (such as&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;; font-style: italic;">example.com</span><span style="font-family: &quot;newbaskerville&quot;;">, or even just&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;; font-style: italic;">.com</span><span style="font-family: &quot;newbaskerville&quot;;">) to agree that for the purpose of future same-origin checks, they want to be considered equivalent.</span><br />
<span style="font-family: &quot;newbaskerville&quot;;"><br /></span>
<span style="font-family: &quot;newbaskerville&quot;;">So basically, if <b>login.example.com</b> and <b>www.example.com </b>both <b>explicitly</b>&nbsp;set their document.domain to example.com, this will lax the same-origin policy checks thereafter.</span><br />
<h2 style="text-align: left;">
<span style="font-family: &quot;arial&quot; , &quot;helvetica&quot; , sans-serif; font-size: x-large;">Exploitation</span></h2>
<span style="font-family: &quot;newbaskerville&quot;;">I was hunting on the Postmates bug bounty program from H1. Now this program has a limited scope. I came across an [[in-scope]] endpoint like the following:&nbsp;</span><br />
<b><br /></b>
<b>https://raster-static.postmates.com/?url=<redacted></redacted></b><br />
<b><br /></b>
Now this endpoint was only was only accepting image links coming from subdomains of postmates.com. I thought, if somehow I can find a subdomain takeover, maybe I can find an SSRF or perhaps serve my own malicious content to the victim. So I went ahead and fired up some subdomain discovery tools and started sifting through them. I found one subdomain, <b>impact.postmates.com</b>, which was prone to a Github subdomain takeover. I went ahead and added the custom domain name to my test repo and the subdomain was mine.<br />
<br />
However, I couldn't have access to any backend, so an SSRF was impossible in this case. The max I could do was serve random JPEG's. Bummer.<br />
<br />
<span style="font-family: &quot;newbaskerville&quot;;">After a lot of time of thinking, something struck my mind. The main domain for users in Postmates is, <b>postmates.com</b>.&nbsp;</span><br />
<span style="font-family: &quot;newbaskerville&quot;;"><br /></span>
<span style="font-family: &quot;newbaskerville&quot;;">Now, "Tangled Web" also states:</span><br />
<span style="font-family: &quot;newbaskerville&quot;;"><br /></span>
<i><span style="font-family: &quot;newbaskerville&quot;;">"</span><span style="font-family: &quot;newbaskerville&quot;;">Simply because l</span><span style="font-family: &quot;newbaskerville&quot;;">ogin.example.com&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">has set its&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">document.domain&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">to&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">example.com&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">does not mean that it will be allowed to access content originating from the website hosted at&nbsp;</span><span style="font-family: &quot;newbaskerville&quot;;">http://example.com/</span><span style="font-family: &quot;newbaskerville&quot;;">. That website needs to perform such an assignment, too, even if common sense would indicate that it is a no-op."</span></i><br />
<i><span style="font-family: &quot;newbaskerville&quot;;"><br /></span></i>
<span style="font-family: &quot;newbaskerville&quot;;">So,&nbsp;</span><i style="font-family: NewBaskerville;">if and only if, </i><span style="font-family: &quot;newbaskerville&quot;;"><b>postmates.com </b>has explicitly set it's document.domain, maybe I could set <b>impact.postmates.com</b>'s document.domain to <b>postmates.com </b>and access it's cookies somehow?</span><br />
<span style="font-family: &quot;newbaskerville&quot;;"><br /></span>
<span style="font-family: &quot;newbaskerville&quot;;"><b>Precursor: </b><i>a.xyz.com</i> can <b>NOT</b>&nbsp;set it's document.domain to <i>a.a.xyz.com </i>or <i>b.xyz.com</i>, but it <b>CAN</b>&nbsp;set it's document.domain to <i>xyz.com</i>. An example of changing the values of blog.takemyhand.xyz to other settings:</span><br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-PNeYO9Suq4s/XOePXQxSYOI/AAAAAAAAAw4/GCJgL1_8L1sRBU14gasWUDPiAkzglZk9wCLcBGAs/s1600/Screenshot%2B2019-05-24%2Bat%2B11.33.38%2BAM.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="130" data-original-width="371" height="112" src="https://3.bp.blogspot.com/-PNeYO9Suq4s/XOePXQxSYOI/AAAAAAAAAw4/GCJgL1_8L1sRBU14gasWUDPiAkzglZk9wCLcBGAs/s320/Screenshot%2B2019-05-24%2Bat%2B11.33.38%2BAM.png" width="320" /></a></div>
<div class="separator" style="clear: both; text-align: left;">
That being said, I went ahead to check if <b>postmates.com </b>had explicitly set their document.domain. They had!&nbsp;<span style="font-family: &quot;newbaskerville&quot;;">Now, you have to keep in mind, that this attack worked only because the main domain of the program was&nbsp;</span><b style="font-family: NewBaskerville;">postmates.com</b><span style="font-family: &quot;newbaskerville&quot;;">&nbsp;(that is, document.domain = <b>postmates.com</b>) and not something like&nbsp;</span><b style="font-family: NewBaskerville;">www.postmates.com (</b><span style="font-family: &quot;newbaskerville&quot;;">document.domain possibly set to <b>www.postmates.com</b>, because then I could not set the document.domain of <b>impact.postmates.com</b> to <b>www.postmates.com</b>).&nbsp;</span>So now all I had to do was, using JS, set the document.domain to <b>postmates.com</b>, and add the following code in my javascript to steal the cookies from the main account:</div>
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-5CFndUHNFD0/XOebNI4kwTI/AAAAAAAAAxQ/6negb6GpcCY7nUH5GZhBXduZPp_ljpHyQCLcBGAs/s1600/Screenshot%2B2019-05-24%2Bat%2B12.50.05%2BPM.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="283" data-original-width="523" height="173" src="https://1.bp.blogspot.com/-5CFndUHNFD0/XOebNI4kwTI/AAAAAAAAAxQ/6negb6GpcCY7nUH5GZhBXduZPp_ljpHyQCLcBGAs/s320/Screenshot%2B2019-05-24%2Bat%2B12.50.05%2BPM.png" width="320" /></a></div>
</div>
</div>
</div>
</div>
</div>
<span style="font-family: &quot;newbaskerville&quot;;">That's it, I could alert the cookies of <b>postmates.com </b>now, and possibly perform an account takeover, since I had complete access to the DOM of the main website.</span><br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-sppyIP2Y0_s/XOerQr8kWDI/AAAAAAAAAxo/TY-bT06xWuU2994R6vjQLyoGbK8NST3SgCLcBGAs/s1600/Screenshot_2019-05-19_at_10.48.38_PM.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="809" data-original-width="1213" height="266" src="https://1.bp.blogspot.com/-sppyIP2Y0_s/XOerQr8kWDI/AAAAAAAAAxo/TY-bT06xWuU2994R6vjQLyoGbK8NST3SgCLcBGAs/s400/Screenshot_2019-05-19_at_10.48.38_PM.png" width="400" /></a></div>
While working on this, I even thought of a few other creative ways to escalate an subdomain takeover to increase it's impact:<br />
<br />
<ul style="text-align: left;">
<li>Bypassing X-Frame-Options : SAMEORIGIN</li>
<li>Bypassing CORS validation where the Access-Control-Allow-Origin is set to *.example.com (* being any subdomain)</li>
<li>Bypassing URL validation wherever applicable (eg. open redirects to steal OAuth login tokens etc.</li>
</ul>
<div>
<br /></div>
<div>
Regards,</div>
<div>
t4kemyh4nd</div>
<br />
<i><span style="font-family: &quot;newbaskerville&quot;;"><br /></span></i>
<i><span style="font-family: &quot;newbaskerville&quot;;"><br /></span></i></div>
</div>
</div>