---
layout: post
title: "[SSTI] Exploiting Go's template engine to get XSS"
date: '2020-06-18T02:48:00.006-07:00'
author: t4kemyh4nd
tags: 
thumbnail: https://miro.medium.com/max/1400/1*GKPQAj1F8EWjn9rMqoVqMg.png
modified_time: '2021-08-30T08:07:48.643-07:00'
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-4252624109805668627
blogger_orig_url: http://blog.takemyhand.xyz/2020/05/ssti-breaking-gos-template-engine-to.html
---

<div id="html" markdown="0">
<div dir="ltr" style="text-align: left;" trbidi="on">
<br /><div dir="ltr" style="text-align: left;" trbidi="on"><b><i><br /></i></b></div><div dir="ltr" style="text-align: left;" trbidi="on"><b><i>This vulnerability is an edge case of user data being passed directly into the template, and the exploit is a result of <i>not</i> using the intended way of rendering templates in Go.</i></b></div><div dir="ltr" style="text-align: left;" trbidi="on"><br /></div><div dir="ltr" style="text-align: left;" trbidi="on">I recently started working with Go and decided to play around with it's in built packages for web related stuff. It's a very neat language which is easy to learn and implement. While looking at the web development side of the language, I came across the package for templates. My first thought was to see if I could get an SSTI to work in it, but sadly I couldn't find any resources online. Through some thorough reading of the templating documentation and some quirks, I was able to successfully achieve XSS. This blogpost signifies the process of finding and exploiting an SSTI in a server written in Go. <br />
<h2 style="text-align: left;">
Introduction to Go's templating engine</h2>
Go provides two templating packages. One is text/template and the other is html/template. The text/template package has no protections for XSS or any kinds of HTML encoding whatsoever. This isn't suited for building web apps, but instead for use in different apps which don't require processing of HTML. The second package, html/template is basically the same as text/template, but with added security protections like HTML encoding and so on.<br />
<br />
For sake of understanding the exact templating used, the following code will be used to demonstrate further attacks:<br />
<script src="https://gist.github.com/t4kemyh4nd/5eb6cd1e6a3c872254502df9cc79a636.js"></script>
<h2 style="text-align: left;">
Detecting and confirming</h2>
Detecting SSTI in Go isn't as simple as sending {{ "{{" }}7*7}} and checking for 49 in the source code. Our first step is going through the documentation to find behavior in templates that is native only to Go- this is done so as to confirm the backend language so that we can focus our payloads only in context of that language. The way to confirm that the template engine used in the backed is Go, following is a non-exhaustive list of payloads and their respective outputs:<br />
<h3 style="text-align: left;">
<ul style="text-align: left;">
<li>{{ "{{" }} . }}&nbsp;</li>
</ul>
</h3>
This will result in an output containing the data struct being passed as input to the template, which in our case is the user1 struct. This can be thought of as the equivalent of {{ "{{" }} self }} in other templating engines. The output in our case would be something like: {1 ameya@gmail.com ameya#123}<br />
<h3 style="text-align: left;">
<ul style="text-align: left;">
<li>{{ "{{" }}printf "%s" "ssti" }}</li>
</ul>
</h3>
In case the above payload doesn't output anything, we can use this payload, which should simply output the string ssti in the response.<br />
<h3 style="text-align: left;">
<ul style="text-align: left;">
<li>{{ "{{" }}html "ssti"}}, {{ "{{" }}js "ssti"}} etc.</li>
</ul>
</h3>
These are a few other payloads which should output the string "ssti" without the trailing words "js" or "html". You can refer to more keywords in the engine <a href="https://golang.org/pkg/text/template">here</a>.<br />
<h2 style="text-align: left;">
Exploitation</h2>
If you have confirmed the above payloads to match Go's behavior, you can now proceed to exploit it to achieve XSS. If the server is using the text/template package, XSS is very easy to achieve by simply providing your payload as input. However, that is not the case with html/template.<br />
<br />
When the server uses the html/template package, all HTML entities are encoded. So, in our case if you try to send <b>/?q={{ "{{" }}"&lt;script&gt;alert(1)&lt;/script&gt;"}}</b>&nbsp;the response will be rendered to <b>&amp;lt;script&amp;gt;alert(1)&amp;lt;/script&amp;gt;</b>.&nbsp;Even going through the documentation doesn't help much as there is no way to render any of our XSS payloads without the default encoding behavior. However, Go allows to DEFINE a whole template and then later call it.<br />
<br />
<b><u>`{{ "{{" }}define "T1"}}ONE{{ "{{" }}end}}{{ "{{" }}template "T1"}}`</u></b><br />
<br />
Using the above behavior, we can introduce an XSS payload in a template, and then get it rendered by simply calling it.<br />
<br />
The payload will be something like:<br />
<br />
<b><u>`{{ "{{" }}define "T1"}}&lt;script&gt;alert(1)&lt;/script&gt;{{ "{{" }}end}} {{ "{{" }}template "T1"}}`</u></b><br />
<b><u><br /></u></b>
  Using this, you should be able to <i>exploit</i> the inbuilt HTML encoder of the html/template package and successfully achieve XSS.</div>
</div>
</div>