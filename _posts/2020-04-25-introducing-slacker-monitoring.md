---
layout: post
title: 'Introducing Slacker: Monitoring subdomain additions in real time and automating
  directory scanning'
date: '2020-04-25T05:59:00.000-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2020-04-25T07:12:07.182-07:00'
thumbnail: https://mk0caiblog1h3pefaf7c.kinstacdn.com/wp-content/uploads/2017/11/cover-slack-01-01.png"
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-477811374770152045
blogger_orig_url: http://blog.takemyhand.xyz/2020/04/introducing-slacker-monitoring.html
---

<div id="html" markdown="0">
<div dir="ltr" style="text-align: left;" trbidi="on">
<span style="font-size: large;">H</span>i everyone, this post signifies the release of Slacker- a Slack bot which alerts you about subdomain additions to assets in realtime, (and also enables you to schedule directory scanning for any websites you may wish to monitor), so you can get an advantage over other bounty hunters when it comes to continuous recon. In this blogpost I will show exactly how to setup the bot, which should take less than 25 minutes. Once you're done, you should get alerts in realtime of whenever a certificate is added, as shown below:<br />
<div style="text-align: center;">
<img border="0" height="568" src="https://3.bp.blogspot.com/-4wa5ppr5kMA/XqQAtQN3ZkI/AAAAAAAAA-A/Y0kUI3tqX9gRg2VXH0UlncO9xbnJ9NlvwCK4BGAYYCw/s640/Screenshot%2B2020-04-25%2Bat%2B2.49.44%2BPM.png" width="640" /></div>
<div>
<h2 style="text-align: left;">
<span style="font-weight: normal;">
Requirements</span></h2>
<ul style="text-align: left;">
<li>Valid FB developer account</li>
<li>Access to Slack API (bots, webhooks etc.)</li>
<li>A valid domain you own</li>
<li>Your own server (i.e. DigitalOcean, AWS instances etc.)</li>
</ul>
<h2 style="text-align: left;">
<span style="font-weight: normal;">Setting up the server</span></h2>
<div>
<ol style="text-align: left;">
<li style="text-align: left;">Setup a free Cloudflare account, add your domains to it, and then point your domain name's A record to the server IP you own by following the instructions (the Cloudflare setup instructions are clear enough to follow), to enable SSL. If you don't have a domain, you can buy one for as low as 1$ a year from GoDaddy, eg. :<a href="http://2.bp.blogspot.com/-spdvZQG_HYM/XqQVHxP8yGI/AAAAAAAAA_E/MHyuJb5R0HcGgrI73ndeDAWdKfDDAd6ZwCK4BGAYYCw/s1600/Screenshot%2B2020-04-25%2Bat%2B4.09.27%2BPM.png" imageanchor="1"><img border="0" height="320" src="https://2.bp.blogspot.com/-spdvZQG_HYM/XqQVHxP8yGI/AAAAAAAAA_E/MHyuJb5R0HcGgrI73ndeDAWdKfDDAd6ZwCK4BGAYYCw/s320/Screenshot%2B2020-04-25%2Bat%2B4.09.27%2BPM.png" width="274" /></a></li>
<li>Clone&nbsp;<a href="https://github.com/t4kemyh4nd/Slacker">https://github.com/t4kemyh4nd/Slacker</a>&nbsp;on your server.</li>
<li>Install mongodb and dirsearch.</li>
<li>Run <i>./install.sh </i>then run <i>python listener.py </i>in the recon-bot directory.</li>
</ol>
<h2 style="text-align: left;">
<span style="font-weight: normal;">Setting up certificate transparency webhooks from Facebook</span></h2>
</div>
<div style="text-align: left;">
We'll be using Facebook's CT API to make this bot. You can read more about it <a href="https://developers.facebook.com/docs/certificate-transparency-api#">here</a>.<br />
<ol style="text-align: left;">
<li>Go to <a href="https://developers.facebook.com/apps/">https://developers.facebook.com/apps/</a>, and create a new app. Fill in the required details etc.</li>
<li>After this, you should be presented with a page with available developer products for use. Choose 'Webhooks' in that list.</li>
<li>In options, select "Certificate Transparency' as shown below, and click on "Subscribe to this object".<a href="https://1.bp.blogspot.com/-b2eeQyKDFkc/XqLj9rLG2yI/AAAAAAAAA8E/9MqLBBnxBiEadMl1LxH__DE5sorLtUDewCLcBGAsYHQ/s1600/Screenshot%2B2020-04-24%2Bat%2B6.31.19%2BPM.png" imageanchor="1" style="clear: left; display: inline !important; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="728" data-original-width="1308" height="356" src="https://1.bp.blogspot.com/-b2eeQyKDFkc/XqLj9rLG2yI/AAAAAAAAA8E/9MqLBBnxBiEadMl1LxH__DE5sorLtUDewCLcBGAsYHQ/s640/Screenshot%2B2020-04-24%2Bat%2B6.31.19%2BPM.png" width="640" /></a></li>
<li>In the following dialog box, enter the value of "Callback URL" as https://{your-domain-here}<your-domain>/subdomain-alert (remember to run listener.py on your server first) and a random value for the "Verify Token" field. You should now be able to save this subscription.</your-domain></li>
<li>In order to receive alerts for real websites, you need to take your app Live. To do this, go to Settings &gt; Basic, and enter the required details. To create a Privacy Policy URL for a website &nbsp;go to <a href="https://app.freeprivacypolicy.com/builder/start/free-privacy-policy">https://app.freeprivacypolicy.com/builder/start/free-privacy-policy</a>, and copy the generated link from there. After this, you should be able to take your app live.</li>
<li>Get your FB App Access Token by issuing the following curl request:&nbsp;curl -X GET "https://graph.facebook.com/oauth/access_token?client_id={your-app-id}&amp;client_secret={your-app-secret}&amp;grant_type=client_credentials"</li>
</ol>
<h2 style="text-align: left;">
<span style="font-weight: normal;">Setting up the Slack bot</span></h2>
<div>
<ol style="text-align: left;">
<li>Got to&nbsp;<a href="https://api.slack.com/apps">https://api.slack.com/apps</a>&nbsp;and create an app for your workspace.</li>
<li>Create 2 channels in your workspace, namely #subdomain-alerts and #dirscan-alerts</li>
<li>In your app settings, on the left hand side, go to "OAuth and permissions" and add the following permissions:<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-H-drACEWNtE/XqLsd1GMI3I/AAAAAAAAA8k/dTuWX5noNmQe0i1UQMczZ1jpW3Onkr3gQCLcBGAsYHQ/s1600/Screenshot%2B2020-04-24%2Bat%2B7.10.25%2BPM.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="527" data-original-width="670" height="500" src="https://1.bp.blogspot.com/-H-drACEWNtE/XqLsd1GMI3I/AAAAAAAAA8k/dTuWX5noNmQe0i1UQMczZ1jpW3Onkr3gQCLcBGAsYHQ/s640/Screenshot%2B2020-04-24%2Bat%2B7.10.25%2BPM.png" width="640" /></a></div>
</li>
<li>Now we need to add an incoming webhook. So on the left hand side again, go to the "Incoming Webhooks" option, and add a webhook for the #dirscan-alerts channel, as shown below. Note down the webhook URL.<a href="https://1.bp.blogspot.com/-i6BwORviO8Y/XqLtlThnJ6I/AAAAAAAAA8w/WNy4matHUDMmktqW7p3RwrPG28epIKfwwCLcBGAsYHQ/s1600/Screenshot%2B2020-04-24%2Bat%2B7.15.22%2BPM.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em; text-align: center;"><img border="0" data-original-height="726" data-original-width="1308" height="352" src="https://1.bp.blogspot.com/-i6BwORviO8Y/XqLtlThnJ6I/AAAAAAAAA8w/WNy4matHUDMmktqW7p3RwrPG28epIKfwwCLcBGAsYHQ/s640/Screenshot%2B2020-04-24%2Bat%2B7.15.22%2BPM.png" width="640" /></a></li>
<li style="text-align: left;">Now we have to add "Slash commands" for the bot, so that we can add, remove or monitor domains by simply sending messages to the bot using Slack itself. Go to the "Slash Commands" section in app settings, and add commands like shown below:<a href="http://3.bp.blogspot.com/-lBFdCsmKUOE/XqQLIrCiWBI/AAAAAAAAA-M/BRkDC_Y0eYsk54r3yBruZ45E-g3nLpzdwCK4BGAYYCw/s1600/Screenshot%2B2020-04-25%2Bat%2B3.34.09%2BPM.png" imageanchor="1" style="text-align: center;"><img border="0" height="554" src="https://3.bp.blogspot.com/-lBFdCsmKUOE/XqQLIrCiWBI/AAAAAAAAA-M/BRkDC_Y0eYsk54r3yBruZ45E-g3nLpzdwCK4BGAYYCw/s640/Screenshot%2B2020-04-25%2Bat%2B3.34.09%2BPM.png" width="640" /></a></li>
<li style="text-align: left;">Now, for every command you create, in the "Edit Command" dialogue, enter the request URL as https://{your-domain-name}<your-domain>/{command-name}<command-name>. That is, for the "/add-domain" command, your request URL will be https://recon.takemyhand.xyz/add-domain, as shown:<a href="http://1.bp.blogspot.com/-lOvdQfwtV1o/XqP-olN1f7I/AAAAAAAAA90/SucYPBoNms4eQT9K83K8T2FAJh3sr_S3ACK4BGAYYCw/s1600/Screenshot%2B2020-04-25%2Bat%2B2.40.47%2BPM.png" imageanchor="1" style="text-align: center;"><img border="0" height="480" src="https://1.bp.blogspot.com/-lOvdQfwtV1o/XqP-olN1f7I/AAAAAAAAA90/SucYPBoNms4eQT9K83K8T2FAJh3sr_S3ACK4BGAYYCw/s640/Screenshot%2B2020-04-25%2Bat%2B2.40.47%2BPM.png" width="640" /></a></command-name></your-domain></li>
</ol>
<div>
That's it! Once this app is installed in your workspace, you can now use it to monitor subdomain additions in realtime! Simply run `<b>python listener.py</b>` and you can now run the app. Here is the list of commands that you can use in your workspace, with what every one of them does:</div>
</div>
<div>
<ul style="text-align: left;">
<li><b>/add-domain&nbsp;{domain-name}</b>:&nbsp;<b>&nbsp;</b>add a domain to monitor, eg. paypal.com, postmates.com etc.</li>
<li><b>/list-domains</b>: list of domains you have added to monitor</li>
<li><b>/remove-domains&nbsp;{domain-name}</b>: remove a domain from the list</li>
<li><b>/add-dirscan {subdomain-name}</b>: add a (sub)domain to monitor for file additions</li>
<li><b>/list-dirscan</b>: list domains added for directory scanning</li>
</ul>
<div>
Currently, the <span style="font-family: inherit;">subdomain</span> additions are updated in realtime; however, the directory scanning is done at a specific time every day. You can change this time by changing the last line of the <b>schedule</b> class instance in the last line of the <b>dirapi.py</b> file. eg. you can change it to&nbsp;<span style="background-color: white;"><span style="font-family: &quot;menlo&quot; , &quot;monaco&quot; , &quot;courier new&quot; , monospace; font-size: 12px; font-weight: bold; white-space: pre;">schedule.every(30).minutes</span><span style="font-family: &quot;menlo&quot; , &quot;monaco&quot; , &quot;courier new&quot; , monospace; font-size: 12px; font-weight: bold; white-space: pre;">.do(</span><span style="font-family: &quot;menlo&quot; , &quot;monaco&quot; , &quot;courier new&quot; , monospace; font-size: 12px; font-weight: bold; white-space: pre;">self</span><span style="font-size: 12px; white-space: pre;"><b style="font-family: Menlo, Monaco, &quot;Courier New&quot;, monospace;">.compareResults) </b></span></span>to monitor directory additions every 30 minutes. You can also add your own custom wordlist by changing the wordlist file in the dirsearch directory.<br />
<br />
Feel free to point out any mistakes that I may have made in the code, or if you have problems with the installation / running of the application.<br />
<br />
</div>
</div>
</div></div>
</div>
</div>
</div>