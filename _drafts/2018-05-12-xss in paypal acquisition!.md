---
layout: post
title: XSS in Paypal acquisition!
date: '2018-05-12T09:33:00.001-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2023-07-16T06:44:53.606-07:00'
thumbnail: https://2.bp.blogspot.com/-1IiDKr7RnMM/WvcXTIm9noI/AAAAAAAAAlU/UPE8DiBI6GEX_JmOPcoffPqMEqPuACuqwCLcBGAs/s72-c/WhatsApp%2BImage%2B2018-05-12%2Bat%2B2.15.14%2BPM.jpeg
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-6262816955796589792
---

<div dir="ltr" style="text-align: left;" trbidi="on">
So I was doing my regular bug hunting about a month ago, and I decided to take up PayPal bug bounty program. One thing I always try to do is target acquisitions in scope, rather than going directly for the main site (go figure).<br />
<br />
So the acquisition is called 'Xoom'. I spent a few hours, looking for low hanging fruits, xss, xsrf, anything. Didn't have any luck. Just as I was about to shut down my laptop, I received an email from Xoom. At the bottom of the email was an 'unsubcribe' button. I decided to test it. Now this is where things get interesting!<br />
If you decide to unsubscribe from the main website, you get to an endpoint like www.xoom.com/unsubscribe?email=ameya@gmail.com. I thought of trying for xss here, as I saw the email param value was getting reflected in the source. No luck!<br />
<br />
But, when you choose to unsubscribe from the email from within the email template, you are taken to an address like refer.xoom.com/?optout=ameya@gmail.com. And this param was vulnerable to XSS, and broken access controls! (I could've inserted a random email address here and have it unsubscribed from Xoom mailing list)<br />
<br />
Takeaways:<br />
<br />
1.) Always go out-of-band.<br />
2.) Don't give up!<br />
<br />
Bounty?<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-1IiDKr7RnMM/WvcXTIm9noI/AAAAAAAAAlU/UPE8DiBI6GEX_JmOPcoffPqMEqPuACuqwCLcBGAs/s1600/WhatsApp%2BImage%2B2018-05-12%2Bat%2B2.15.14%2BPM.jpeg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="265" data-original-width="1125" height="75" src="https://2.bp.blogspot.com/-1IiDKr7RnMM/WvcXTIm9noI/AAAAAAAAAlU/UPE8DiBI6GEX_JmOPcoffPqMEqPuACuqwCLcBGAs/s320/WhatsApp%2BImage%2B2018-05-12%2Bat%2B2.15.14%2BPM.jpeg" width="320" /></a></div>
<br />
<br />
Regards,<br />
t4kemyh4nd</div>
