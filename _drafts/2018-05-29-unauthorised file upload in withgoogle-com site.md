---
layout: post
title: Unauthorised file upload in withgoogle.com site
date: '2018-05-29T01:29:00.001-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2023-07-16T06:44:47.540-07:00'
thumbnail: https://3.bp.blogspot.com/-t-V7m88shsY/Ww0OmEqtnVI/AAAAAAAAAow/ZMUZt36GVYwZ3fgYTnM4SJayHJyGx4KjACLcBGAs/s72-c/cspoc2.png
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-6745528518244454937
---

<div dir="ltr" style="text-align: left;" trbidi="on">
Greetings,<br />
<br />
A few days ago I found a bug in a site csfirst.withgoogle.com, which was due to a misconfigured Drupal instance.<br />
<br />
After signing up on the site, there was a lot of functionality on it. I decided to test the user profile tab (looking for low hanging fruits like CSRF etc.). Sadly I couldn't find anything important. But looking at my dirsearch results and robots.txt file, I saw quite some interesting endpoints. The original settings page for user profile looked like this:<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-t-V7m88shsY/Ww0OmEqtnVI/AAAAAAAAAow/ZMUZt36GVYwZ3fgYTnM4SJayHJyGx4KjACLcBGAs/s1600/cspoc2.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="900" data-original-width="1600" height="180" src="https://3.bp.blogspot.com/-t-V7m88shsY/Ww0OmEqtnVI/AAAAAAAAAow/ZMUZt36GVYwZ3fgYTnM4SJayHJyGx4KjACLcBGAs/s320/cspoc2.png" width="320" /></a></div>
<br />
As you can see, there is nothing interesting here. However, the robots.txt file of the same site had an endpoint `/user/register/`. Going to that endpoint revealed the following functionality:<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-0qFIirC0TXM/Ww0O1yP4EkI/AAAAAAAAAo0/VhWBLZTMmwAAcDN_F6xm4Noi9q7ExYKcACLcBGAs/s1600/cspoc.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="900" data-original-width="1600" height="180" src="https://2.bp.blogspot.com/-0qFIirC0TXM/Ww0O1yP4EkI/AAAAAAAAAo0/VhWBLZTMmwAAcDN_F6xm4Noi9q7ExYKcACLcBGAs/s320/cspoc.png" width="320" /></a></div>
<br />
From there I could upload files upto 30MB in size and gifs, txt files etc. which would get uploaded on the storage server. Many other sensitive options are also available. This was a result of their Drupal instance being configured improperly.<br />
<br />
<br />
Reward: enlisted in Google Honorable Mentions <br />
<br />
<br />
Thanks for reading,<br />
t4kemyh4nd</div>
