---
layout: post
title: Private video info disclosure through API in Vimeo!
date: '2018-05-13T08:28:00.000-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2023-07-16T06:44:51.484-07:00'
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-8146381508449526448
---

<div dir="ltr" style="text-align: left;" trbidi="on">
<img src="https://images.wallpapersden.com/image/download/vimeo-video-hosting-russia_24081_2048x2048.jpg" style="display: none;" />
<br />
<div dir="ltr" style="text-align: left;" trbidi="on">
Greetings,<br />
<br />
In this blog post, I'm gonna talk about a bug I found through API testing in the video sharing app Vimeo.<br />
<br />
Private videos, as the name suggests, are supposed to be completely private. For an app to see a user's private videos, the access token needs to have the 'private' scope. You guessed it, scope-based testing.<br />
<br />
So I create an access token with public scope, and request the endpoint `/me/videos/&lt;private-video-id&gt;`. And voila, all metadata was leaked.<br />
<br />
Regards,<br />
t4kemyh4nd</div>
</div>
