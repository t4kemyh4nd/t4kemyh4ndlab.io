---
layout: post
title: Got my OSCP cert!
date: '2018-12-16T02:43:00.000-08:00'
author: t4kemyh4nd
tags: 
modified_time: '2023-07-06T07:24:46.505-07:00'
thumbnail: https://1.bp.blogspot.com/-m8CfZ_RsqaI/XBYszzY2QLI/AAAAAAAAAuI/8jXKhq6ZnnItV2WUmQt47NZsq7ZjBwXrwCLcBGAs/s72-c/badge.PNG
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-5933862977251734930
---

<div dir="ltr" style="text-align: left;" trbidi="on">
Greetings,<br />
<br />
It's been almost half a year since I've posted anything related to security- no bug bounties, no findings etc. This is because since the past 6 months I had been preparing for the wicked PWK course, to get my OSCP certificate.<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-m8CfZ_RsqaI/XBYszzY2QLI/AAAAAAAAAuI/8jXKhq6ZnnItV2WUmQt47NZsq7ZjBwXrwCLcBGAs/s1600/badge.PNG" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="351" data-original-width="593" height="189" src="https://1.bp.blogspot.com/-m8CfZ_RsqaI/XBYszzY2QLI/AAAAAAAAAuI/8jXKhq6ZnnItV2WUmQt47NZsq7ZjBwXrwCLcBGAs/s320/badge.PNG" width="320" /></a></div>
<br />
<br />
The prestigious OSCP needs no introduction. You’ve probably read about how tough, demanding and punishing the PWK course and labs are; about the three big boxes in the IT department; and about the time and hard work you have to put in to get this cert. I’m not going to be talking about the course itself, or how much time I spent in the labs. This blog post is going to be more about how I managed to pass the exam on my 2nd attempt, and my mindset throughout the exam preparation stage.<br />
<br />
I signed up for 30 days of lab access, which was my first mistake. For someone like me, who had no idea about penetration testing, and only knew about web hacking, 30 days is not a good choice. If you’re someone like me, go for the 60 days and you can get an extension after that accordingly.<br />
By the end of these 30 days, I hardly rooted around 2-3 boxes. Although I now had a a fair idea about how it worked.<br />
<br />
After those 30 days, I was pretty baffled as to what I was going to do next. Obviously, I was far from ready for the exam. This was the time when I signed up for HackTheBox VIP. And that was one of the best decisions I’d made. I learnt so much from the retired boxes and from ippsec’s videos too. I was focusing more on boxes with difficulty “not too easy” to “medium”. And to anyone wanting to do OSCP I’d say boxes with this range of difficulty is perfect for preparation. Once I got a hang of rooting boxes, I took a lab extension of 60 days, and rooted all the boxes under 50 days. By now I was pretty confident that I would crack the exam in my first attempt.<br />
<br />
Fast forward to 11th November, 2:30 A.M, the scheduled time for my exam. The exam was not a proctored one. I was really anxious when the exam started. However, I managed to get 67.5 points, and couldn’t make any progress after that. I had 67.5 points and 8 hours to escalate privileges on one box, and get user shell on the remaining one box. 8 hours went by, and I couldn’t do shit. There I was, stuck at 67.5 points, and dependent on my lab report for the remaining extra 5 points. As I was writing the report, I realized I didn't have the screenshot for one box showing it's proof.txt. Boom, now no chance of passing even with the extra 5 points of the lab report. I wrote and submitted the report nonetheless, only to hear 2 days later that I didn't clear my exam. I was prepared for it.<br />
<br />
As soon as I got the email that I hadn't clear the exam, I bought an exam schedule link, and scheduled my exam for the 13th of December, 8:30 P.M. In this period of one month, I was really worried. Let me tell you why. I'm in my 2nd year of engineering at the time of writing this. I had messed up my semester to the point of having to repeat it, because I was preparing for OSCP. So now this was my last chance. If I messed up this time, I'd be left without a certificate, and I would've messed up my academics for nothing. I was back to HackTheBox. I was trying to keep a pace of one box per day, and spent sleepless nights trying to find those hideous root.txt's, on the boxes labeled "hard" this time.<br />
<br />
Exam day. I was really anxious yet again. But this time I knew that I was up against. I knew I had to take a screenshot of every small thing I came across. I knew I was going to record my screen during the entire buffer overflow box. The exam started, this time a proctored one. I had 80 points under my belt in just 4 hours. I was left with just one box, worth 20 points. It was a windows box, and for me privilege escalation on a windows machine has always been a nightmare. However, I went to sleep that night at 2:30 A.M., and woke up the next day at 8:30. I had almost 12 hours, and only 20 points to score. I was determined not to fail this time. Even though I knew I had already passed the exam, I wanted to score the maximum this time. I remembered how I was stuck for 8 hours straight the last time. So I spent 4 hours on the box, struggling to get a user shell. Enumerated everything I could, but couldn't find anything. And then it struck me. Boom, another 10 points down the bottle. I now had a total of 90 points. I was satisfied with myself. I didn't even try to escalate privileges on this box.<br />
<br />
After getting those 90 points, I checked and rechecked and repeated the same cycle for 2 hours. Went through all the screenshots, all the notes. I kept checking that the proof files were correctly pasted into the control panel. Once I was satisfied, I went out for a few hours, celebrated with my best friend, but I knew that the report was still to be written. I came back late at night, and started writing my report very, very carefully. It took me about 4 hours to write a report, which I thought was perfect. I read the report several times, made sure all screenshots were attached, because I knew OffSec are very strict when it comes to the exam documentation. So by 3:30 A.M. that night, on 15th of December , I submitted my report. I waited and waited and waited for the email.<br />
<br />
Surely enough, I got the email within 36 hours. Now I'm OSCP.<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-G1f6fH0S5XA/XBYpgEs-9CI/AAAAAAAAAtw/rXykIq_S2D8V9JGMBQ9BlR4BQ9FAmX2FACLcBGAs/s1600/oscp.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="292" data-original-width="1370" height="85" src="https://2.bp.blogspot.com/-G1f6fH0S5XA/XBYpgEs-9CI/AAAAAAAAAtw/rXykIq_S2D8V9JGMBQ9BlR4BQ9FAmX2FACLcBGAs/s400/oscp.png" width="400" /></a></div>
<br />
To anyone taking the OSCP exam, I'd like to give away few tips:<br />
<br />
1. It's okay to fail the first time if you're a beginner like me. Even if you're not prepared for the exam, take it. It will give you an idea of how the exam is, how difficult the boxes are.<br />
2. HackTheBox is the most important thing. Get a VIP subscription right now if you haven't already. I didn't do VulnHub or any other sites. Just HTB and the OSCP labs.<br />
3. Screenshots. Take screenshots of literally anything, you never know which step you might've missed. I recommend, recording the entire the screen for the duration of the exam.<br />
4. Check for bad characters in the buffer overflow machine more than 4-5 times if your exploit isn't working. And remember NOP sleds. I messed up the first time on my BOF box because I was simply missing NOP sleds in my payload.<br />
5. I didn't do any other departments in the OSCP labs, I focused only on the IT department, and that too not on boxes that required information from post-exploitation of other boxes. I cracked the first 30-35, and left the last ones.<br />
6. Pivoting. If you're worried about pivoting, port forwarding etc., and are new to these concepts, it's fine. You don't need pivoting in the examination.<br />
<br />
I'd like to thank all the people that helped me over these past months. My roommate Vedant who stayed up those nights watching me Hack Those Boxes. My teacher and friend Shashank, who literally taught me hacking and got me introduced to this field. My friend Aditya, who did HTB with me and taught me many things in pentesting. My brother Yash who always backed me up for messing up my college. And most thankful to, of course, my loving parents without whom none of this would've been possible.<br />
<br />
Good night,<br />
t4kemyh4nd</div>
