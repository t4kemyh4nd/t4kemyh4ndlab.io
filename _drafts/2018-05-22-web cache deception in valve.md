---
layout: post
title: Web cache deception in Valve
date: '2018-05-22T21:44:00.001-07:00'
author: t4kemyh4nd
tags: 
modified_time: '2023-07-16T06:44:50.276-07:00'
thumbnail: https://1.bp.blogspot.com/-1yHe4w5dKAI/WwTw4-_npeI/AAAAAAAAAnE/MeLkm-dGLSwBR-ND5IrnCng1SO5KYymrACLcBGAs/s72-c/valvepoc.png
blogger_id: tag:blogger.com,1999:blog-6833997159700243932.post-2011996394162292912
---

<div dir="ltr" style="text-align: left;" trbidi="on">
Greetings,<br />
<br />
Last night I was hunting bugs in Valve bug bounty program, and I came across an interesting bug, called 'web cache deception attack'.<br />
<br />
For knowing what this attack is, please visit https://www.blackhat.com/docs/us-17/wednesday/us-17-Gil-Web-Cache-Deception-Attack-wp.pdf.<br />
<br />
So I visited the site partner.steamgames.com and saw it had a login functionality.<br />
I logged in, while running dirsearch in the background. I couldn't find any interesting bugs right there, but looking at my dirsearch results, I saw that two of the results returned the exact same result, namely:<br />
<br />
partner.steamgames.com/documentation and partner.steamgames.com/documentation/config.yml<br />
<br />
Now I had read about web cache deception a few days ago, and it struck me. So I go to a page with user info like partner.steamgames.com/home and then partner.steamgames.com/home/tmh.css. Both returned the same result! So all conditions for WCD attack are fulfilled.<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-1yHe4w5dKAI/WwTw4-_npeI/AAAAAAAAAnE/MeLkm-dGLSwBR-ND5IrnCng1SO5KYymrACLcBGAs/s1600/valvepoc.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="900" data-original-width="1600" height="180" src="https://1.bp.blogspot.com/-1yHe4w5dKAI/WwTw4-_npeI/AAAAAAAAAnE/MeLkm-dGLSwBR-ND5IrnCng1SO5KYymrACLcBGAs/s320/valvepoc.png" width="320" /></a></div>
<br />
<br />
Next I open up a private window, and visit partner.steamgames.com/home/tmh.css, and voila :)<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-YGFYfRc0qm0/WwTw-KtlkHI/AAAAAAAAAnI/SnveVMQ3K_ICUp4K3dBUfM6sXktCuG1DwCLcBGAs/s1600/valvepoc2.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="900" data-original-width="1600" height="180" src="https://4.bp.blogspot.com/-YGFYfRc0qm0/WwTw-KtlkHI/AAAAAAAAAnI/SnveVMQ3K_ICUp4K3dBUfM6sXktCuG1DwCLcBGAs/s320/valvepoc2.png" width="320" /></a></div>
Now all I have to do is get the victim to visit a page displaying sensitive info with a malformed URL, and then I can access it. Also, any web sites sitting behind the same reverse proxy will be affected by this bug.<br />
<br />
Bounty? 750$ + 150$<br />
<br />
Always keep reading everything and every white paper you come across on the internet.<br />
<br />
Regards,<br />
t4kemyh4nd<br />
<br />
<br /></div>
